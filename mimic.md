project: MiMiC
project_dir: ./src
output_dir: ./doc
summary: Multiscale modeling in CPMD (MiMiC) library.
fpp_extensions: F90
predocmark: >
docmark_alt: #
predocmark_alt: <
graph: true
display: public
display: protected
display: private
source: false
search: true
macro: TEST LOGIC=.true.
project_github: https://gitlab.com/mimic-cpmd/mimic