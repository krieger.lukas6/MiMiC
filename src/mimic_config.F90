!
!    MiMiC: A Framework for Multiscale Modeling in Computational Chemistry
!    Copyright (C) 2021  	  Jógvan Magnus Haugaard Olsen,
!                             Viacheslav Bolnykh,
!                             Simone Meloni,
!                             Emiliano Ippoliti,
!                             and Ursula Röthlisberger.
!
!    This file is part of MiMiC.
!
!    MiMiC is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as
!    published by the Free Software Foundation, either version 3 of
!    the License, or (at your option) any later version.
!
!    MiMiC is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!

module mimic_config
implicit none
character(len=*), parameter :: PROJECT_NAME = "@PROJECT_NAME@"
character(len=*), parameter :: PROJECT_VER = "@PROJECT_VERSION@"
character(len=*), parameter :: PROJECT_VER_MAJOR = "@PROJECT_VERSION_MAJOR@"
character(len=*), parameter :: PROJECT_VER_MINOR = "@PROJECT_VERSION_MINOR@"
character(len=*), parameter :: PROJECT_VER_PATCH = "@PROJECT_VERSION_PATCH@"
end module mimic_config