\chapter{Pre- and Postprocessing}
\label{Ch:prepostprocessing}

In QM/MM one has to avoid ``double counting'', that is avoid that both classical and quantum force
fields act on any atom (here we are not excluding the QM/MM interface forces and energies).
We propose to do this in the spirit of a loose coupling between codes. This consists in preparing
two MM calculations, one for the entire system (including the QM part), and one only for the QM part.
Thus, the pure MM part of forces, energies, etc.\ is obtained by subtracting results from the
MM calculation of only the QM part from the MM calculation of the entire system.
This will still include double counting of QM-MM electrostatic interactions (as can be seen below),
which can be removed by setting the electrostatic parameters for the QM atoms to zero in the
force field for both MM calculations.

Assume a system with 3 in the QM part (S1) and 3 atoms in the MM part (S2), and that they are
covalently bound in a string, i.e.\ M3-M2-M1-Q1-Q2-Q3. We name the QM atoms Q1, Q2, etc.\ and
the MM atoms M1, M2, etc., according to their distance from the interface. The energy terms are denoted as
follows, $B$ for bonded, $A$ for angle, $D$ for dihedral, $Q$ for electrostatic and W for
repulsion-dispersion (e.g. Lennard-Jones potential). Considering only QM-MM interaction terms
(that can be described as QM/MM or MM), we have the following total energy
\begin{displaymath}
E^{}_{\textrm{tot}} = E^{\textrm{MM}}_{\textrm{S1+S2}} - E^{\textrm{MM}}_{\textrm{S1}} + E^{\textrm{QM-MM}}_{\textrm{S1+S2}} + E^{\textrm{QM}}_{\textrm{S1}} \ .
\end{displaymath}
The MM energy of the total system is
\begin{align*}
E^{\textrm{MM}}_{\textrm{S1+S2}} =& B^{}_{\textrm{M3-M2}} + B^{}_{\textrm{M2-M1}} + B^{}_{\textrm{M1-Q1}} + B^{}_{\textrm{Q1-Q2}} + B^{}_{\textrm{Q2-Q3}} \\
& + A^{}_{\textrm{M3-M2-M1}} + A^{}_{\textrm{M2-M1-Q1}} + A^{}_{\textrm{M1-Q1-Q2}} + A^{}_{\textrm{Q1-Q2-Q3}} \\
& + D^{}_{\textrm{M3-M2-M1-Q1}} + D^{}_{\textrm{M2-M1-Q1-Q2}} + D^{}_{\textrm{M1-Q1-Q2-Q3}} \\
& + W^{}_{\textrm{M3-Q1}} + W^{}_{\textrm{M3-Q2}} + W^{}_{\textrm{M3-Q3}} + W^{}_{\textrm{M2-Q2}} + W^{}_{\textrm{M2-Q3}} + W^{}_{\textrm{M1-Q3}} \\
& + Q^{}_{\textrm{M3-Q1}} + Q^{}_{\textrm{M3-Q2}} + Q^{}_{\textrm{M3-Q3}} + Q^{}_{\textrm{M2-Q2}} + Q^{}_{\textrm{M2-Q3}} + Q^{}_{\textrm{M1-Q3}} \\
\end{align*}
assuming that 1-2 and 1-3 non-bonded interactions are neglected and that 1-4 interactions are fully included.
The MM energy of the QM part is
\begin{align*}
E^{\textrm{MM}}_{\textrm{S1}} =& B^{}_{\textrm{Q1-Q2}} + B^{}_{\textrm{Q2-Q3}} + A^{}_{\textrm{Q1-Q2-Q3}} \ ,
\end{align*}
and the QM-MM interaction energy is
\begin{align*}
E^{\textrm{QM-MM}}_{\textrm{S1+S2}} =& Q^{*}_{\textrm{M3-Q1}} + Q^{*}_{\textrm{M3-Q2}} + Q^{*}_{\textrm{M3-Q3}} + Q^{*}_{\textrm{M2-Q2}} + Q^{*}_{\textrm{M2-Q3}} + Q^{*}_{\textrm{M1-Q3}} \ ,
\end{align*}
where the star indicates mixed QM-MM to differentiate from the pure MM electrostatics. The QM energy of the
QM part is obtained using the usual density functional approximation or wave function method.
The total energy thus becomes
\begin{align*}
E^{}_{\textrm{tot}} =& B^{}_{\textrm{M3-M2}} + B^{}_{\textrm{M2-M1}} + B^{}_{\textrm{M1-Q1}} + B^{}_{\textrm{Q1-Q2}} + B^{}_{\textrm{Q2-Q3}} \\
& + A^{}_{\textrm{M3-M2-M1}} + A^{}_{\textrm{M2-M1-Q1}} + A^{}_{\textrm{M1-Q1-Q2}} + A^{}_{\textrm{Q1-Q2-Q3}} \\
& + D^{}_{\textrm{M3-M2-M1-Q1}} + D^{}_{\textrm{M2-M1-Q1-Q2}} + D^{}_{\textrm{M1-Q1-Q2-Q3}} \\
& + W^{}_{\textrm{M3-Q1}} + W^{}_{\textrm{M3-Q2}} + W^{}_{\textrm{M3-Q3}} + W^{}_{\textrm{M2-Q2}} + W^{}_{\textrm{M2-Q3}} + W^{}_{\textrm{M1-Q3}} \\
& + Q^{}_{\textrm{M3-Q1}} + Q^{}_{\textrm{M3-Q2}} + Q^{}_{\textrm{M3-Q3}} + Q^{}_{\textrm{M2-Q2}} + Q^{}_{\textrm{M2-Q3}} + Q^{}_{\textrm{M1-Q3}} \\
& - B^{}_{\textrm{Q1-Q2}} - B^{}_{\textrm{Q2-Q3}} - A^{}_{\textrm{Q1-Q2-Q3}} \\
& + Q^{*}_{\textrm{M3-Q1}} + Q^{*}_{\textrm{M3-Q2}} + Q^{*}_{\textrm{M3-Q3}} + Q^{*}_{\textrm{M2-Q2}} + Q^{*}_{\textrm{M2-Q3}} + Q^{*}_{\textrm{M1-Q3}} \\
& + E^{\textrm{QM}}_{\textrm{S1}} \\
=& B^{}_{\textrm{M3-M2}} + B^{}_{\textrm{M2-M1}} + B^{}_{\textrm{M1-Q1}} \\
& + A^{}_{\textrm{M3-M2-M1}} + A^{}_{\textrm{M2-M1-Q1}} + A^{}_{\textrm{M1-Q1-Q2}} \\
& + D^{}_{\textrm{M3-M2-M1-Q1}} + D^{}_{\textrm{M2-M1-Q1-Q2}} + D^{}_{\textrm{M1-Q1-Q2-Q3}} \\
& + W^{}_{\textrm{M3-Q1}} + W^{}_{\textrm{M3-Q2}} + W^{}_{\textrm{M3-Q3}} + W^{}_{\textrm{M2-Q2}} + W^{}_{\textrm{M2-Q3}} + W^{}_{\textrm{M1-Q3}} \\
& + Q^{}_{\textrm{M3-Q1}} + Q^{}_{\textrm{M3-Q2}} + Q^{}_{\textrm{M3-Q3}} + Q^{}_{\textrm{M2-Q2}} + Q^{}_{\textrm{M2-Q3}} + Q^{}_{\textrm{M1-Q3}} \\
& + Q^{*}_{\textrm{M3-Q1}} + Q^{*}_{\textrm{M3-Q2}} + Q^{*}_{\textrm{M3-Q3}} + Q^{*}_{\textrm{M2-Q2}} + Q^{*}_{\textrm{M2-Q3}} + Q^{*}_{\textrm{M1-Q3}} \\
& + E^{\textrm{QM}}_{\textrm{S1}} \ ,
\end{align*}
where it can be seen that there is a double-counting of electrostatic interactions between QM and MM.
By removing (or zeroing) the electrostatic parameters of the QM part in the calculation of $E^{\textrm{MM}}_{\textrm{S1+S2}}$ and $E^{\textrm{MM}}_{\textrm{S1}}$, we get the following total energy
\begin{align*}
E^{}_{\textrm{tot}} =& B^{}_{\textrm{M3-M2}} + B^{}_{\textrm{M2-M1}} + B^{}_{\textrm{M1-Q1}} \\
& + A^{}_{\textrm{M3-M2-M1}} + A^{}_{\textrm{M2-M1-Q1}} + A^{}_{\textrm{M1-Q1-Q2}} \\
& + D^{}_{\textrm{M3-M2-M1-Q1}} + D^{}_{\textrm{M2-M1-Q1-Q2}} + D^{}_{\textrm{M1-Q1-Q2-Q3}} \\
& + W^{}_{\textrm{M3-Q1}} + W^{}_{\textrm{M3-Q2}} + W^{}_{\textrm{M3-Q3}} + W^{}_{\textrm{M2-Q2}} + W^{}_{\textrm{M2-Q3}} + W^{}_{\textrm{M1-Q3}} \\
& + Q^{*}_{\textrm{M3-Q1}} + Q^{*}_{\textrm{M3-Q2}} + Q^{*}_{\textrm{M3-Q3}} + Q^{*}_{\textrm{M2-Q2}} + Q^{*}_{\textrm{M2-Q3}} + Q^{*}_{\textrm{M1-Q3}} \\
& + E^{\textrm{QM}}_{\textrm{S1}} \ ,
\end{align*}
in which there is no double-counting of the electrostatic interactions.
