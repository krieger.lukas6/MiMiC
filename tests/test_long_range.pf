!
!    MiMiC: A Framework for Multiscale Modeling in Computational Chemistry
!    Copyright (C) 2021       Jógvan Magnus Haugaard Olsen,
!                             Viacheslav Bolnykh,
!                             Simone Meloni,
!                             Emiliano Ippoliti,
!                             and Ursula Röthlisberger.
!
!    This file is part of MiMiC.
!
!    MiMiC is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as
!    published by the Free Software Foundation, either version 3 of
!    the License, or (at your option) any later version.
!
!    MiMiC is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!

module test_long_range

    use pFUnit, only: testcase, sourcelocation, anyexceptions, &
                      assertequal
    use mimic_precision, only: dp
    use mimic_particles
    use mimic_fragments
    use mimic_subsystems
    use mimic_cells
    use mimic_field_grids
    use mimic_properties
    use mimic_tensors, only: initialize_tensors, terminate_tensors, &
                             polytensor_size
    use mimic_long_range, only: compute_lr_potential,&
                                compute_lr_energy,&
                                compute_lr_forces,&
                                compute_folded_tensors

    implicit none

    real(dp), parameter :: TOLERANCE = 1.0e-14_dp

    @testcase
    type, extends(testcase) :: test_lr_type
        ! quantum fragment variables
        real(dp), dimension(:,:,:), allocatable :: cpmd_tau_qm
        real(dp), dimension(:,:,:), allocatable :: cpmd_density
        real(dp), dimension(:,:,:), allocatable :: quantum_forces
        real(dp), dimension(:,:,:), allocatable :: quantum_potential
        type(quantum_fragment_type) :: quantum_fragment
        ! long range fragment variables
        real(dp), dimension(:,:), allocatable :: cpmd_coord_mm
        real(dp), dimension(:,:), allocatable :: fragment_forces
        type(subsystem_type) :: subsystem
    contains
        procedure :: setup
        procedure :: teardown
    end type test_lr_type

contains

subroutine setup(this)

    class(test_lr_type), intent(inout) :: this

    integer :: lu
    integer :: i, j, k
    integer :: num_sp_qm
    integer :: num_atoms_qm
    integer :: nr1, nr2, nr3
    integer, dimension(3) :: num_points
    integer, dimension(:), allocatable :: num_at_qm
    real(dp), dimension(3) :: origin
    real(dp), dimension(3,3) :: lattice
    real(dp), dimension(:), allocatable :: cpmd_cell_dim
    real(dp), dimension(:), allocatable :: cpmd_charges_qm
    type(cell_type) :: cell
    type(density_type) :: density
    type(potential_type) :: potential
    type(nucleus_type), dimension(:), allocatable :: nuclei
    integer :: num_atoms_mm
    integer :: num_fragments
    integer, dimension(:), allocatable :: fragment_definitions
    real(dp), dimension(:), allocatable :: cpmd_charges_mm
    type(atom_type), dimension(:), allocatable :: atoms
    type(multipoles_type) :: dummy

    open(newunit=lu, file='./setup/acetone_water/quantum_fragment', status='old')
    rewind(lu)
    read(lu, '(i12)') num_sp_qm
    allocate(num_at_qm(num_sp_qm))
    do i = 1, num_sp_qm
        read(lu, '(i12)') num_at_qm(i)
    end do
    allocate(this%cpmd_tau_qm(3,maxval(num_at_qm),num_sp_qm))
    this%cpmd_tau_qm = 0.0_dp
    do i = 1, num_sp_qm
        do j = 1, num_at_qm(i)
            read(lu, '(3d24.16)') this%cpmd_tau_qm(1:3,j,i)
        end do
    end do
    allocate(cpmd_charges_qm(num_sp_qm))
    cpmd_charges_qm = 0.0_dp
    do i = 1, num_sp_qm
        read(lu, '(d24.16)') cpmd_charges_qm(i)
    end do
    allocate(cpmd_cell_dim(3))
    read(lu, '(3d24.16)') cpmd_cell_dim
    read(lu, '(3i12)') nr1, nr2, nr3
    allocate(this%cpmd_density(nr1,nr2,nr3))
    do i = 1, nr3
        do j = 1, nr2
            do k = 1, nr1
                read(lu, '(d24.16)') this%cpmd_density(k,j,i)
            end do
        end do
    end do
    close(lu)
    this%cpmd_density = - this%cpmd_density

    open(newunit=lu, file='./setup/acetone_water/long_range_fragments', status='old')
    rewind(lu)
    read(lu, '(i12)') num_atoms_mm
    allocate(this%cpmd_coord_mm(3,num_atoms_mm))
    do i = 1, num_atoms_mm
        read(lu, '(3d24.16)') this%cpmd_coord_mm(:,i)
    end do
    allocate(cpmd_charges_mm(num_atoms_mm))
    do i = 1, num_atoms_mm
        read(lu, '(d24.16)') cpmd_charges_mm(i)
    end do
    close(lu)

    num_atoms_qm = 0
    do i = 1, num_sp_qm
        do j = 1, num_at_qm(i)
            num_atoms_qm = num_atoms_qm + 1
        end do
    end do

    allocate(nuclei(num_atoms_qm))

    allocate(this%quantum_forces(3,maxval(num_at_qm),num_sp_qm))
    this%quantum_forces = 0.0_dp

    allocate(this%quantum_potential(nr1,nr2,nr3))
    this%quantum_potential = 0.0_dp

    k = 1
    do i = 1, num_sp_qm
        do j = 1, num_at_qm(i)
            call nuclei(k)%init(k, i, j, cpmd_charges_qm(i), &
                                this%cpmd_tau_qm(:,j,i), &
                                this%quantum_forces(:,j,i), .false.)
            k = k + 1
        end do
    end do

    num_points(1) = nr1
    num_points(2) = nr2
    num_points(3) = nr3

    lattice = 0.0_dp
    lattice(1,1) = cpmd_cell_dim(1)
    lattice(2,2) = cpmd_cell_dim(2) * cpmd_cell_dim(1)
    lattice(3,3) = cpmd_cell_dim(3) * cpmd_cell_dim(1)

    origin = 0.0_dp

    call cell%init(1, num_points, num_points, origin, lattice)

    call density%init(1, this%cpmd_density)

    call potential%init(1, this%quantum_potential)

    call this%quantum_fragment%init(1, nuclei, cell, density, potential)

    allocate(this%fragment_forces(3,num_atoms_mm))
    this%fragment_forces = 0.0_dp

    ! assuming water molecules, i.e. 3 atoms
    num_fragments = num_atoms_mm / 3
    allocate(atoms(num_atoms_mm))
    allocate(fragment_definitions(0:num_fragments))
    fragment_definitions(0) = 0
    k = 1
    do i = 1, num_fragments
        do j = 1, 3
            call atoms(k)%init(k, i, j, cpmd_charges_mm(k), 0.8_dp, this%cpmd_coord_mm(:,k), &
                               this%fragment_forces(:,k), dummy, .false.)
            k = k + 1
        end do
        fragment_definitions(i) = k - 1
    end do

    call this%subsystem%init(1, 1.0_dp, atoms)

    call this%subsystem%define_fragments(num_fragments, fragment_definitions)

    call initialize_tensors()

end subroutine setup

subroutine teardown(this)

    class(test_lr_type), intent(inout) :: this

    deallocate(this%cpmd_tau_qm)
    deallocate(this%cpmd_density)
    deallocate(this%quantum_forces)
    deallocate(this%quantum_potential)
    deallocate(this%cpmd_coord_mm)
    deallocate(this%fragment_forces)
    deallocate(this%subsystem%fragments)

    call terminate_tensors()

end subroutine teardown

@test
subroutine quantum_fragment_multipoles(this)

    class(test_lr_type), intent(inout) :: this

    integer :: expansion_order
    integer :: start_index, end_index
    real(dp), dimension(3) :: origin
    real(dp), dimension(:), allocatable :: multipoles
    real(dp), dimension(:), allocatable :: multipoles_reference

    expansion_order = 5

    start_index = 1
    end_index = this%quantum_fragment%cell%num_points(1)

    allocate(multipoles(polytensor_size(expansion_order)))
    allocate(multipoles_reference(polytensor_size(expansion_order)))

                            ! zeroth order
    multipoles_reference = [-4.7961634663806763E-013_dp, &
                            ! first order
                            -1.1198926714683299_dp, -1.4114083860801863_dp, 0.59617420404092769_dp, &
                            ! second order
                            -17.798906027855203_dp, 2.7994251482106449_dp, -2.4111773964601717_dp, &
                            -15.627840705176673_dp, -1.9945411510000675_dp, -20.689617410634753_dp, &
                            ! third order
                            8.3999319454931509_dp, 0.14269145683663709_dp, -2.8312308701101259_dp, &
                            -1.6818120174919500_dp, 3.2426527134576801_dp, 1.0862003562428768_dp, &
                            10.141445402577190_dp, 0.79947009129332258_dp, 1.1917426564219937_dp, &
                            -5.3518612521586491_dp, &
                            ! fourth order
                            -273.97975338666669_dp, -52.789817245999643_dp, -68.970218164480983_dp, &
                            -82.171347200116998_dp, -5.4206751579421564_dp, -133.75929843481100_dp, &
                            -47.163598905704845_dp, -32.597942817853777_dp, -20.345936517131953_dp, &
                            -66.196921296304112_dp, -225.83073104649645_dp, 4.1325856359310933_dp, &
                            -122.53319844651460_dp, 7.4817935467847718_dp, -537.92657803343343_dp, &
                            ! fifth order
                            145.78014981209060_dp, 145.10717685294529_dp, -129.08984580718828_dp, &
                            101.71734758150910_dp, -68.796968768810416_dp, -13.570419056741116_dp, &
                            89.970030591821228_dp, -28.953815817410032_dp, 15.244026925651852_dp, &
                            -72.560955757047836_dp, 172.74066201975091_dp, -65.263197290318942_dp, &
                            36.702768425188992_dp, -91.762624757274821_dp, 32.661843360069746_dp, &
                            311.40078442548918_dp, -131.16367951677790_dp, 40.882830968673360_dp, &
                            -73.769858315216538_dp, -27.837040718030110_dp, 201.98049533957783_dp]

    origin = compute_centroid(this%quantum_fragment%nuclei)

    call this%quantum_fragment%compute_electronic_multipoles(origin, expansion_order, &
                                                             start_index, end_index)
    call this%quantum_fragment%compute_nuclear_multipoles(origin, expansion_order)

    multipoles = this%quantum_fragment%electronic_multipoles%values + &
                 this%quantum_fragment%nuclear_multipoles%values

    @assertEqual(multipoles_reference, multipoles, TOLERANCE*2.0e4_dp)

    deallocate(multipoles)
    deallocate(multipoles_reference)

end subroutine quantum_fragment_multipoles

@test
subroutine long_range_energy(this)

    class(test_lr_type), intent(inout) :: this

    integer :: expansion_order
    integer :: start_index, end_index
    real(dp) :: energy
    real(dp) :: energy_reference
    real(dp), dimension(3) :: origin
    real(dp), dimension(:), allocatable :: tensors, tensor_sums

    expansion_order = 5

    start_index = 1
    end_index = this%quantum_fragment%cell%num_points(1)

    energy_reference = -0.35630298598814381_dp

    origin = compute_centroid(this%quantum_fragment%nuclei)

    call this%quantum_fragment%compute_electronic_multipoles(origin, expansion_order, &
                                                             start_index, end_index)
    call this%quantum_fragment%compute_nuclear_multipoles(origin, expansion_order)

    allocate(tensors(size(this%quantum_fragment%nuclear_multipoles%values)))
    allocate(tensor_sums(size(this%quantum_fragment%nuclear_multipoles%values)))

    call compute_folded_tensors(tensors, tensor_sums, &
                                origin, expansion_order, &
                                this%subsystem%atoms)

    call compute_lr_energy(this%quantum_fragment, energy, &
                           tensor_sums)

    @assertEqual(energy_reference, energy, TOLERANCE)

end subroutine long_range_energy

@test
subroutine long_range_energy_dist(this)

    class(test_lr_type), intent(inout) :: this

    integer :: atom_index
    integer :: expansion_order
    integer :: start_index, end_index
    real(dp) :: energy_reference
    real(dp) :: energy, temp_energy
    real(dp), dimension(3) :: origin
    real(dp), dimension(:), allocatable :: tensors1, tensors2, tensor_sums1, tensor_sums2

    expansion_order = 5

    start_index = 1
    end_index = this%quantum_fragment%cell%num_points(1)

    energy_reference = -0.35630298598814381_dp

    origin = compute_centroid(this%quantum_fragment%nuclei)

    atom_index = size(this%subsystem%atoms) / 2

    call this%quantum_fragment%compute_electronic_multipoles(origin, expansion_order, &
                                                             start_index, end_index)
    call this%quantum_fragment%compute_nuclear_multipoles(origin, expansion_order)

    allocate(tensors1(size(this%quantum_fragment%nuclear_multipoles%values)))
    allocate(tensors2(size(this%quantum_fragment%nuclear_multipoles%values)))
    allocate(tensor_sums1(size(this%quantum_fragment%nuclear_multipoles%values)))
    allocate(tensor_sums2(size(this%quantum_fragment%nuclear_multipoles%values)))

    call compute_folded_tensors(tensors1, tensor_sums1, &
                                origin, expansion_order, &
                                this%subsystem%atoms(1:atom_index))

    call compute_folded_tensors(tensors2, tensor_sums2, &
                                origin, expansion_order, &
                                this%subsystem%atoms(atom_index + 1:size(this%subsystem%atoms)))

    call compute_lr_energy(this%quantum_fragment, &
                           energy, tensor_sums1 + tensor_sums2)

    energy = energy! + temp_energy

    @assertEqual(energy_reference, energy, TOLERANCE)

end subroutine long_range_energy_dist

@test
subroutine long_range_potential(this)

    class(test_lr_type), intent(inout) :: this

    integer :: lu
    integer :: i, j, k
    integer :: expansion_order
    integer :: start_index, end_index
    integer, dimension(3) :: num_points
    real(dp), dimension(3) :: origin
    real(dp), dimension(:,:,:), allocatable :: potential_reference
    real(dp), dimension(:), allocatable :: tensors, tensor_sums

    expansion_order = 5

    start_index = 1
    end_index = this%quantum_fragment%cell%num_points(1)

    num_points = this%quantum_fragment%cell%num_points

    open(newunit=lu, file='./setup/acetone_water/quantum_fragment_potential', status='old')
    rewind(lu)
    allocate(potential_reference(num_points(1),num_points(2),num_points(3)))
    do i = 1, num_points(3)
        do j = 1, num_points(2)
            do k = 1, num_points(1)
                read(lu, '(d24.16)') potential_reference(k,j,i)
            end do
        end do
    end do
    close(lu)

    origin = compute_centroid(this%quantum_fragment%nuclei)

    call this%quantum_fragment%compute_electronic_multipoles(origin, expansion_order, &
                                                             start_index, end_index)
    call this%quantum_fragment%compute_nuclear_multipoles(origin, expansion_order)

    allocate(tensors(size(this%quantum_fragment%nuclear_multipoles%values)))
    allocate(tensor_sums(size(this%quantum_fragment%nuclear_multipoles%values)))

    call compute_folded_tensors(tensors, tensor_sums, &
                                origin, expansion_order, &
                                this%subsystem%atoms)

    call compute_lr_potential(this%quantum_fragment, &
                              1, this%quantum_fragment%cell%num_points(1), &
                              -tensor_sums)

    @assertEqual(potential_reference, this%quantum_fragment%potential%field, TOLERANCE)

    deallocate(potential_reference)

end subroutine long_range_potential

@test
subroutine long_range_forces(this)

    class(test_lr_type), intent(inout) :: this

    integer :: lu
    integer :: i, j, k
    integer :: num_atoms_mm
    integer :: expansion_order
    integer :: start_index, end_index
    real(dp), dimension(3) :: origin
    real(dp), dimension(:,:), allocatable :: qm_forces_reference
    real(dp), dimension(:,:), allocatable :: mm_forces_reference

    expansion_order = 5

    start_index = 1
    end_index = this%quantum_fragment%cell%num_points(1)

    open(newunit=lu, file='./setup/acetone_water/quantum_fragment_forces', status='old')
    rewind(lu)
    allocate(qm_forces_reference(1:3,this%quantum_fragment%num_nuclei))
    do i = 1, this%quantum_fragment%num_nuclei
        read(lu, '(3d24.16)') qm_forces_reference(1:3,i)
    end do
    close(lu)

    num_atoms_mm = 0
    do i = 1, this%subsystem%num_fragments
        num_atoms_mm = num_atoms_mm + this%subsystem%fragments(i)%num_atoms
    end do
    open(newunit=lu, file='./setup/acetone_water/long_range_fragments_forces', status='old')
    rewind(lu)
    allocate(mm_forces_reference(1:3,num_atoms_mm))
    do i = 1, num_atoms_mm
        read(lu, '(3d24.16)') mm_forces_reference(1:3,i)
    end do
    close(lu)

    origin = compute_centroid(this%quantum_fragment%nuclei)

    call this%quantum_fragment%compute_electronic_multipoles(origin, expansion_order, &
                                                             start_index, end_index)
    call this%quantum_fragment%compute_nuclear_multipoles(origin, expansion_order)

    call compute_lr_forces(this%subsystem%atoms, this%quantum_fragment, 1, size(this%subsystem%atoms))

    do i = 1, this%quantum_fragment%num_nuclei
        @assertEqual(qm_forces_reference(:,i), this%quantum_fragment%nuclei(i)%force, TOLERANCE)
    end do

    k = 1
    do i = 1, this%subsystem%num_fragments
        do j = 1, this%subsystem%fragments(i)%num_atoms
            @assertEqual(mm_forces_reference(:,k), this%subsystem%fragments(i)%atoms(j)%force, TOLERANCE)
            k = k + 1
        end do
    end do

end subroutine long_range_forces

subroutine long_range_forces_dist(this)

    class(test_lr_type), intent(inout) :: this

    integer :: lu
    integer :: i, j, k
    integer :: num_atoms_mm
    integer :: atom_index
    integer :: expansion_order
    integer :: start_index, end_index
    real(dp), dimension(3) :: origin
    real(dp), dimension(:,:), allocatable :: qm_forces_reference
    real(dp), dimension(:,:), allocatable :: mm_forces_reference

    expansion_order = 5

    start_index = 1
    end_index = this%quantum_fragment%cell%num_points(1)

    open(newunit=lu, file='./setup/acetone_water/quantum_fragment_forces', status='old')
    rewind(lu)
    allocate(qm_forces_reference(1:3,this%quantum_fragment%num_nuclei))
    do i = 1, this%quantum_fragment%num_nuclei
        read(lu, '(3d24.16)') qm_forces_reference(1:3,i)
    end do
    close(lu)

    num_atoms_mm = 0
    do i = 1, this%subsystem%num_fragments
        num_atoms_mm = num_atoms_mm + this%subsystem%fragments(i)%num_atoms
    end do
    open(newunit=lu, file='./setup/acetone_water/long_range_fragments_forces', status='old')
    rewind(lu)
    allocate(mm_forces_reference(1:3,num_atoms_mm))
    do i = 1, num_atoms_mm
        read(lu, '(3d24.16)') mm_forces_reference(1:3,i)
    end do
    close(lu)

    origin = compute_centroid(this%quantum_fragment%nuclei)

    call this%quantum_fragment%compute_electronic_multipoles(origin, expansion_order, &
                                                             start_index, end_index)
    call this%quantum_fragment%compute_nuclear_multipoles(origin, expansion_order)

    atom_index = size(this%subsystem%atoms)

    call compute_lr_forces(this%subsystem%atoms, this%quantum_fragment, 1, atom_index)
    call compute_lr_forces(this%subsystem%atoms, this%quantum_fragment, atom_index + 1, size(this%subsystem%atoms))

    do i = 1, this%quantum_fragment%num_nuclei
        @assertEqual(qm_forces_reference(:,i), this%quantum_fragment%nuclei(i)%force, TOLERANCE)
    end do

    k = 1
    do i = 1, this%subsystem%num_fragments
        do j = 1, this%subsystem%fragments(i)%num_atoms
            @assertEqual(mm_forces_reference(:,k), this%subsystem%fragments(i)%atoms(j)%force, TOLERANCE)
            k = k + 1
        end do
    end do

end subroutine long_range_forces_dist

end module test_long_range
