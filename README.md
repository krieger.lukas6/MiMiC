# MiMiC: A Framework for Multiscale Modeling in Computational Chemistry

## Authors

[Jógvan Magnus Haugaard Olsen](mailto:jmho@kemi.dtu.dk) [DTU - Technical University of Denmark]

[Viacheslav Bolnykh](mailto:v.bolnykh@fz-juelich.de) [Forschungszentrum Jülich]

## Contributors

Simone Meloni [University of Ferrara]

Emiliano Ippoliti [Forschungszentrum Jülich]

Paolo Carloni [Forschungszentrum Jülich]

Ursula Röthlisberger [École Polytechnique Fédérale de Lausanne]

## License

Copyright (C) 2015-2021  Jógvan Magnus Haugaard Olsen,
                         Viacheslav Bolnykh,
                         Simone Meloni,
                         Emiliano Ippoliti,
                         Paolo Carloni,
                         and Ursula Röthlisberger.

MiMiC is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3 of
the License, or (at your option) any later version.

MiMiC is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

## Citation

Please cite these papers if you use MiMiC in your work:
1. J. M. H. Olsen, V. Bolnykh, S. Meloni, E. Ippoliti, M. P. Bircher, P. Carloni, U. Rothlisberger,
   _MiMiC: A Novel Framework for Multiscale Modeling in Computational Chemistry_,
   J. Chem. Theory Comput. **15**, 3810–3823 (2019).
   DOI: [10.1021/acs.jctc.9b00093](https://doi.org/10.1021/acs.jctc.9b00093)
2. V. Bolnykh, J. M. H. Olsen, S. Meloni, M. P. Bircher, E. Ippoliti, P. Carloni, U. Rothlisberger,
   _Extreme Scalability of DFT-Based QM/MM MD Simulations Using MiMiC_,
   J. Chem. Theory Comput. **15**, 5601–5613 (2019).
   DOI: [10.1021/acs.jctc.9b00424](https://doi.org/10.1021/acs.jctc.9b00424)
