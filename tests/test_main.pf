!
!    MiMiC: A Framework for Multiscale Modeling in Computational Chemistry
!    Copyright (C) 2021       Jógvan Magnus Haugaard Olsen,
!                             Viacheslav Bolnykh,
!                             Simone Meloni,
!                             Emiliano Ippoliti,
!                             and Ursula Röthlisberger.
!
!    This file is part of MiMiC.
!
!    MiMiC is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as
!    published by the Free Software Foundation, either version 3 of
!    the License, or (at your option) any later version.
!
!    MiMiC is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!

module test_main

    use pFUnit
    use mimic_precision
    use mimic_types
    use mimic_particles
    use mimic_fragments
    use mimic_cells
    use mimic_field_grids
    use mimic_properties
    use mimic_subsystems
    use mimic_main
    use mpi, only: MPI_COMM_WORLD
    use mcl_fortran, only: mcl_init, mcl_handshake, mcl_send, &
                          mcl_receive, mcl_destroy
    use mcl_requests

    implicit none

    @TestCase
    type, extends(MPITestCase) :: test_main_type
        real(dp) :: tolerance = 1e-15_dp
        integer :: num_clients = 2
        integer :: local_comm
        character(len=7), dimension(2) :: paths = ['./test1', './test2']
        integer :: ierr

        integer, dimension(:), allocatable :: ref_ids, ref_atom_types
        real(kind=dp), dimension(:), allocatable :: ref_charges, ref_masses
        real(kind=dp), dimension(:,:), allocatable :: ref_coordinates
        integer :: ref_atoms_num, ref_species_num

        integer :: atoms_number, maps_number

        integer, dimension(3) :: ref_atoms_pcode
        integer, dimension(3) :: ref_frags_pcode
        integer, dimension(:), allocatable :: bonds_pcode
        integer, dimension(:), allocatable :: angles_pcode
        integer, dimension(:), allocatable :: ref_bonds_pcode
        integer, dimension(:, :, :), allocatable :: ref_bonds
        real(kind=dp), dimension(:, :), allocatable :: ref_lenghts
        class(bond_type), dimension(:, :), allocatable :: bonds
        class(angle_type), dimension(:, :), allocatable :: angles
        integer, dimension(:, :), allocatable :: ref_atoms_pfragment
        integer, dimension(:, :), allocatable :: overlaps
        integer :: num_atoms, num_species, num_q_atoms, num_q_species
        integer, dimension(:), allocatable :: atoms_pcode_test
        integer, dimension(:), allocatable :: ref_multipole_orders
        integer :: n_client, n_atom, n_species, n_map
        real(dp), dimension(:,:,:), allocatable :: tau, tau_ref, force
        integer :: total_species
        integer, dimension(:), allocatable :: atoms_pspecies
        real(dp), dimension(:), allocatable :: factors
        type(maps_type), dimension(:), allocatable :: overlap_maps
        real(dp), dimension(:), allocatable :: cov_radii
        integer, dimension(:), allocatable :: atid, spid
        type(sizes_type) :: sizes
        type(system_type) :: system_data

    contains
        procedure :: setUp
        procedure :: tearDown

    end type test_main_type

contains

subroutine setUp(this)

    class(test_main_type), intent(inout) :: this

    integer :: n_client, n_atom, n_species, n_map
    integer :: n_angle, n_bond
    integer :: qcode_species, qcode_atoms
    integer :: unit
    integer :: rank

    rank = this%getProcessRank()

    open(newunit=unit, file='./setup/data_collect/test.xyz', status="old", action="read")
    read(unit, *) this%atoms_number
    read(unit, *)
    allocate(this%ref_ids(this%atoms_number))
    allocate(this%ref_atom_types(this%atoms_number))
    allocate(this%ref_coordinates(3, this%atoms_number))
    allocate(this%ref_charges(this%atoms_number))
    allocate(this%ref_masses(this%atoms_number))
    do n_atom = 1, this%atoms_number
        read(unit, *) this%ref_ids(n_atom), this%ref_atom_types(n_atom),&
            this%ref_coordinates(1:3, n_atom), this%ref_charges(n_atom), this%ref_masses(n_atom)
    end do
    close(unit)

    open(newunit=unit, file='./setup/data_collect/input', status="old", action="read")
    read(unit, *) this%ref_atoms_pcode
    read(unit, *) this%ref_frags_pcode
    allocate(this%ref_atoms_pfragment(maxval(this%ref_frags_pcode), 3))
    this%ref_atoms_pfragment = -1
    do n_client = 1, 3
        read(unit, *) this%ref_atoms_pfragment(1:this%ref_frags_pcode(n_client), n_client)
    end do

    read(unit, *) this%maps_number
    allocate(this%overlaps(4, this%maps_number))

    do n_map = 1, this%maps_number
        read(unit, *) this%overlaps(:, n_map)
    end do

    close(unit)

    open(newunit=unit, file='./setup/data_collect/ref_sizes', status="old", action="read")
    read(unit, *) this%ref_atoms_num, this%ref_species_num
    close(unit)

    allocate(this%overlap_maps(this%num_clients))

    do n_client = 1, this%num_clients
        do n_map = 1, size(this%overlaps, 2)
            if (this%overlaps(1, n_map) == n_client + 1) then
                call this%overlap_maps(n_client)%add_entry(this%overlaps(2, n_map), &
                     this%overlaps(3, n_map), this%overlaps(4, n_map))
            end if
        end do ! n_map
    end do

    this%num_q_atoms = 0
    this%num_q_species = 0
    qcode_species = maxval(this%ref_atom_types(1:this%ref_atoms_pcode(1)))

    do n_species  = 1, qcode_species
        qcode_atoms = size(pack(this%ref_atom_types(1:this%ref_atoms_pcode(1)),this%ref_atom_types(1:this%ref_atoms_pcode(1)) == n_species))
        if (qcode_atoms > 0) then
            this%num_q_species = this%num_q_species + 1
        end if

        if (qcode_atoms > this%num_q_atoms) then
            this%num_q_atoms = qcode_atoms
        end if
    end do ! n_species

    allocate(this%factors(this%num_clients))
    this%factors(:) = 1

    open(newunit=unit, file='./setup/data_collect/tau_ref', status="old", action="read")
    read(unit, *) this%num_species
    allocate(this%atoms_pspecies(this%num_species))
    read(unit, *) this%atoms_pspecies
    allocate(this%tau_ref(3, maxval(this%atoms_pspecies), this%num_species))
    this%tau_ref(:,:,:) = 0
    do n_species = 1, this%num_species
        read(unit, *)
        do n_atom = 1, this%atoms_pspecies(n_species)
            read(unit, *) this%tau_ref(1:3, n_atom, n_species)
        end do
    end do
    close(unit)

    allocate(this%ref_multipole_orders(this%num_clients))
    this%ref_multipole_orders(:) = 0

    allocate(this%cov_radii(this%num_species))
    this%cov_radii(:) = 1.0_dp

    allocate(this%force(3, maxval(this%atoms_pspecies), this%num_species))

    allocate(this%atid(size(this%ref_ids)))
    allocate(this%spid(size(this%ref_ids)))
    open(newunit=unit, file='./setup/data_collect/tau_map', status="old", action="read")
    do n_atom = 1, size(this%ref_ids)
        read(unit, *) this%atid(n_atom), this%spid(n_atom)
    end do
    close(unit)

    allocate(this%bonds_pcode(this%num_clients))
    allocate(this%angles_pcode(this%num_clients))

    open(newunit=unit, file='./setup/data_collect/top', status="old", action="read")
    read(unit, *) this%bonds_pcode(1:this%num_clients)
    allocate(this%bonds(maxval(this%bonds_pcode), this%num_clients))
    do n_client = 1, this%num_clients
        do n_bond = 1, this%bonds_pcode(n_client)
            read(unit, *) this%bonds(n_bond, n_client)%atom_i, &
                       this%bonds(n_bond, n_client)%atom_j, &
                       this%bonds(n_bond, n_client)%length
        enddo ! n_bond
    end do ! n_client

    read(unit, *) this%angles_pcode(1:this%num_clients)
    allocate(this%angles(maxval(this%angles_pcode), this%num_clients))
    do n_client = 1, this%num_clients
        do n_angle = 1, this%angles_pcode(n_client)
            read(unit, *) this%angles(n_angle, n_client)%atom_i, &
                       this%angles(n_angle, n_client)%atom_j, &
                       this%angles(n_angle, n_client)%atom_k, &
                       this%angles(n_angle, n_client)%angle
        enddo ! n_bond
    end do ! n_client
    close(unit)

    this%local_comm = MPI_COMM_WORLD
    call mcl_init(this%local_comm)

    if (rank == 0) then
        call mimic_handshake(this%paths)
        call mimic_init_overlaps(this%overlaps)
    else
        call mcl_handshake(this%paths(rank), ';', .FALSE.)
    endif

end subroutine setUp

subroutine tearDown(this)

    class(test_main_type), intent(inout) :: this

    integer :: n_client

    deallocate(this%ref_ids)
    deallocate(this%ref_atom_types)
    deallocate(this%ref_coordinates)
    deallocate(this%ref_charges)
    deallocate(this%ref_masses)
    deallocate(this%ref_atoms_pfragment)
    deallocate(this%overlaps)
    deallocate(this%overlap_maps)
    deallocate(this%factors)
    deallocate(this%tau_ref)
    deallocate(this%ref_multipole_orders)
    deallocate(this%cov_radii)
    deallocate(this%force)

    if (this%getProcessRank() == 0) then
        call mimic_destroy
    else
        call mcl_destroy
    endif


end subroutine tearDown

@Test(npes = [3])
subroutine testMM(this)

    class(test_main_type), intent(inout) :: this

    integer :: rank, ierr
    integer :: command
    integer :: offset = 1
    integer :: n, n_client, n_fragment, n_atom, n_const, n_bond
    type(atom_type) :: atom
    type(subsystem_type), dimension(2) :: subsystems
    real(dp), dimension(:,:,:), target, allocatable :: tau, force
    integer, dimension(:), allocatable :: index_buffer
    real(kind=dp), dimension(:), allocatable :: length_buffer
    integer, dimension(2) :: ref_num = [5, 3]
    integer, dimension(5, 3) :: ref

    type(bond_type), dimension(3, 2) :: ref_bonds
    integer :: at_i, at_j

    ref_bonds(1, 1)%atom_i = 32
    ref_bonds(1, 1)%atom_j = 33
    ref_bonds(2, 1)%atom_i = 37
    ref_bonds(2, 1)%atom_j = 38
    ref_bonds(3, 1)%atom_i = 41
    ref_bonds(3, 1)%atom_j = 2

    ref_bonds(1, 2)%atom_i = 2
    ref_bonds(1, 2)%atom_j = 45

    ref(:, 1) = [1, 4, 15, 23, 2]
    ref(:, 2) = [6, 1, 9, -1, -1]

    rank = this%getProcessRank()
    if (rank == 0) then
        allocate(tau(3, maxval(this%atoms_pspecies), this%num_species))
        allocate(force(3, maxval(this%atoms_pspecies), this%num_species))
        tau(:,1:this%num_q_atoms,1:this%num_q_species) = &
         this%tau_ref(:,1:this%num_q_atoms,1:this%num_q_species)

        call mimic_request_sizes(this%sizes, this%system_data)

        call mimic_request_system_data(this%sizes, this%system_data)

        deallocate(this%sizes%nbonds_pcode)
        allocate(this%sizes%nbonds_pcode(2))
        this%sizes%nbonds_pcode(1) = 3
        this%sizes%nbonds_pcode(2) = 1

        deallocate(this%system_data%bonds)
        allocate(this%system_data%bonds(3, 2))

        this%system_data%bonds(1, 1)%atom_i = 13
        this%system_data%bonds(1, 1)%atom_j = 14
        this%system_data%bonds(2, 1)%atom_i = 18
        this%system_data%bonds(2, 1)%atom_j = 19
        this%system_data%bonds(3, 1)%atom_i = 22
        this%system_data%bonds(3, 1)%atom_j = 1

        this%system_data%bonds(1, 2)%atom_i = 1
        this%system_data%bonds(1, 2)%atom_j = 2

        call mimic_allocate_mm_struct(subsystems, this%factors, tau, &
             force, this%atoms_pspecies(1:this%num_q_species), &
             this%num_species, this%cov_radii, this%sizes, this%system_data)

        offset = 0
        do n_client = 1, size(subsystems)
            offset = offset + this%ref_atoms_pcode(n_client)
            do n_fragment = 1, size(subsystems(n_client)%fragments)
                do n_atom = 1, size(subsystems(n_client)%fragments(n_fragment)%atoms)
                    atom = subsystems(n_client)%fragments(n_fragment)%atoms(n_atom)

                    @assertAssociated(atom%coordinate, tau(1:3, this%atid(atom%id + offset), this%spid(atom%id + offset)))
                    @assertAssociated(atom%force, force(1:3, this%atid(atom%id + offset), this%spid(atom%id + offset)))
                    @assertEqual(this%ref_charges(atom%id + offset), atom%charge)
                    @assertEqual(this%cov_radii(this%spid(atom%id + offset)), atom%radius)
                end do
            end do

            do n_bond = 1, this%sizes%nbonds_pcode(n_client)
                at_i = this%system_data%bonds(n_bond, n_client)%atom_i
                at_j = this%system_data%bonds(n_bond, n_client)%atom_j
                @assertEqual(ref_bonds(n_bond, n_client)%atom_i, at_i)
                @assertEqual(ref_bonds(n_bond, n_client)%atom_j, at_j)
            end do !n_bond
        end do
    else
        do n = 1, rank
            offset = offset + this%ref_atoms_pcode(n)
        end do

        ! sizes
        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_NUM_ATOMS, command)
        call mcl_send(this%ref_atoms_pcode(rank + 1), 1, MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_NUM_ATOM_TYPES, command)
        call mcl_send(ref_num(rank), 1, MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_ATOM_SPECIES, command)
        call mcl_send(this%ref_atom_types(offset : offset + this%ref_atoms_pcode(rank + 1) - 1), &
            & this%ref_atoms_pcode(rank + 1), MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_MULTIPOLE_ORDER, command)
        call mcl_send(0, 1, MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_NUM_FRAGMENTS, command)
        call mcl_send(this%ref_frags_pcode(rank + 1), 1, MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_NUM_ATOMS_IN_FRAGMENTS, command)
        call mcl_send(this%ref_atoms_pfragment(1:this%ref_frags_pcode(rank + 1), rank + 1), &
            & this%ref_frags_pcode(rank + 1), MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_NUM_BONDS, command)
        call mcl_send(this%bonds_pcode(rank), 1, MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_NUM_ANGLES, command)
        call mcl_send(this%angles_pcode(rank), 1, MCL_DATA, 0)

        allocate(index_buffer(2 * this%bonds_pcode(rank)))
        allocate(length_buffer(this%bonds_pcode(rank)))

        do n_const = 1, this%bonds_pcode(rank)
            index_buffer(n_const * 2 - 1) = this%bonds(n_const, rank)%atom_i
            index_buffer(n_const * 2) = this%bonds(n_const, rank)%atom_j
            length_buffer(n_const) = this%bonds(n_const, rank)%length
        end do

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_BOND_ATOMS, command)
        call mcl_send(index_buffer, this%bonds_pcode(rank) * 2, MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_BOND_LENGTHS, command)
        call mcl_send(length_buffer, this%bonds_pcode(rank), MCL_DATA, 0)
        deallocate(index_buffer)
        deallocate(length_buffer)

        allocate(index_buffer(3 * this%angles_pcode(rank)))
        allocate(length_buffer(this%angles_pcode(rank)))

        do n_const = 1, this%angles_pcode(rank)
            index_buffer(n_const * 3 - 2) = this%angles(n_const, rank)%atom_i
            index_buffer(n_const * 3 - 1) = this%angles(n_const, rank)%atom_j
            index_buffer(n_const * 3) = this%angles(n_const, rank)%atom_k
            length_buffer(n_const) = this%angles(n_const, rank)%angle
        end do

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_ANGLE_ATOMS, command)
        call mcl_send(index_buffer, this%angles_pcode(rank) * 3, MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_ANGLE_VALS, command)
        call mcl_send(length_buffer, this%angles_pcode(rank), MCL_DATA, 0)


        ! system data
        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_ATOM_MULTIPOLES, command)
        call mcl_send(this%ref_charges(offset : offset + this%ref_atoms_pcode(rank + 1) - 1), &
            & this%ref_atoms_pcode(rank + 1), MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_ATOM_MASSES, command)
        call mcl_send(this%ref_masses(offset : offset + ref_num(rank) - 1), &
            & ref_num(rank), MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_ATOMS_IN_FRAGMENTS, command)
        call mcl_send(this%ref_ids(offset : offset + this%ref_atoms_pcode(rank + 1) - 1), &
            & this%ref_atoms_pcode(rank + 1), MCL_DATA, 0)

        call mcl_receive(command, 1, MCL_COMMAND, 0)
        @assertEqual(MCL_SEND_ATOM_ELEMENTS, command)
        call mcl_send(ref(:, rank), ref_num(rank), MCL_DATA, 0)
    end if

end subroutine testMM

@Test(npes = [3])
subroutine testQM(this)

    class(test_main_type), intent(inout) :: this
    real(dp), dimension(3) :: origin
    real(dp), dimension(3,3) :: box
    real(dp), dimension(10,10,10), target :: rho, extf
    integer, dimension(3) :: n_points
    real(dp), dimension(:,:,:), target, allocatable :: tau, force
    integer :: n_nucleus
    type(nucleus_type) :: nucleus
    type(quantum_fragment_type) :: quantum_fragment

    origin(:) = 0
    n_points(:) = 10
    box(1,1) = 1
    box(2,2) = 1
    box(3,3) = 1
    box(1,2) = 0
    box(1,3) = 0
    box(2,1) = 0
    box(2,3) = 0
    box(3,1) = 0
    box(3,2) = 0

    allocate(tau(3, maxval(this%atoms_pspecies(1:this%num_q_atoms)), this%num_q_species))
    allocate(force(3, maxval(this%atoms_pspecies(1:this%num_q_atoms)), this%num_q_species))

    call mimic_allocate_qm_struct(quantum_fragment, this%atoms_pspecies(1:this%num_q_atoms), &
            this%ref_charges(1:this%ref_atoms_pcode(1)), &
            origin, box, n_points, n_points, rho, extf, tau, force)

    @assertEqual(origin, quantum_fragment%cell%origin)
    @assertEqual(box, quantum_fragment%cell%lattice)
    @assertAssociated(quantum_fragment%density%field, rho)
    @assertAssociated(quantum_fragment%potential%field, extf)
    do n_nucleus = 1, quantum_fragment%num_nuclei
        nucleus = quantum_fragment%nuclei(n_nucleus)
        @assertAssociated(nucleus%coordinate, tau(1:3, this%atid(nucleus%id), this%spid(nucleus%id)))
        @assertAssociated(nucleus%force, force(1:3, this%atid(nucleus%id), this%spid(nucleus%id)))
    end do

end subroutine testQM

@Test(npes = [3])
subroutine testCenter(this)

    class(test_main_type), intent(inout) :: this

    type(nucleus_type), dimension(4) :: nuclei
    type(atom_type), dimension(8) :: atoms
    real(dp), dimension(3,3) :: box
    real(dp), dimension(3,8) :: coords, ref_coords
    real(dp), dimension(3) :: force_dum
    real(dp), dimension(1,1,1) :: field
    type(multipoles_type) :: multipoles
    integer :: n_nucleus, n_atom
    type(quantum_fragment_type) :: quantum_fragment
    type(subsystem_type), dimension(1) :: subsystems
    type(density_type) :: density
    type(cell_type) :: cell
    real(dp), dimension(3) :: rdiff, rdiff_ref


    box(:, :) = 0.0_dp
    box(1, 1) = 1.0_dp
    box(2, 2) = 1.0_dp
    box(3, 3) = 1.0_dp

    coords(:, 1) = [2.0_dp, 2.0_dp, 2.0_dp]
    coords(:, 2) = [3.0_dp, 2.0_dp, 2.0_dp]
    coords(:, 3) = [2.0_dp, 3.0_dp, 2.0_dp]
    coords(:, 4) = [2.0_dp, 3.0_dp, 3.0_dp]
    coords(:, 5) = [0.0_dp, 0.0_dp, 0.0_dp]
    coords(:, 6) = [0.0_dp, 1.0_dp, 0.0_dp]
    coords(:, 7) = [0.0_dp, 0.0_dp, 1.0_dp]
    coords(:, 8) = [1.0_dp, 0.0_dp, 1.0_dp]

    ref_coords(:, 1) = [0.0_dp, 0.0_dp, 0.0_dp]
    ref_coords(:, 2) = [1.0_dp, 0.0_dp, 0.0_dp]
    ref_coords(:, 3) = [0.0_dp, 1.0_dp, 0.0_dp]
    ref_coords(:, 4) = [0.0_dp, 1.0_dp, 1.0_dp]
    ref_coords(:, 5) = [-2.0_dp, -2.0_dp, -2.0_dp]
    ref_coords(:, 6) = [-2.0_dp, -1.0_dp, -2.0_dp]
    ref_coords(:, 7) = [-2.0_dp, -2.0_dp, -1.0_dp]
    ref_coords(:, 8) = [-1.0_dp, -2.0_dp, -1.0_dp]

    rdiff_ref = [-2.0_dp, -2.0_dp, -2.0_dp]

    do n_nucleus = 1, 4
        call nuclei(n_nucleus)%init(1, 1, 1, 1.0_dp, coords(:, n_nucleus), force_dum, .false.)
        call atoms(n_nucleus)%init(1, 1, 1, 1.0_dp, 1.0_dp, coords(:, n_nucleus), force_dum, multipoles, .true.)
    enddo

    do n_atom = 1, 4
        call atoms(4 + n_atom)%init(1, 1, 1, 1.0_dp, 1.0_dp, coords(:, 4 + n_atom), force_dum, multipoles, .false.)
    enddo

    call cell%init(0, [1, 1, 1], [1, 1, 1], [0.0_dp, 0.0_dp, 0.0_dp], box)
    call density%init(0, field)
    call quantum_fragment%init(id=1, nuclei=nuclei, cell=cell, density=density)
    call subsystems(1)%init(0, 1.0_dp, atoms)
    call subsystems(1)%define_fragments(2, [0, 4, 8])
    call mimic_center_qm(rdiff, subsystems, quantum_fragment)
    @assertEqual(rdiff_ref, rdiff, this%tolerance)
    do n_atom = 1, 8
        @assertEqual(ref_coords(:, n_atom), coords(:, n_atom), this%tolerance)
    end do

end subroutine testCenter

@Test(npes = [3])
subroutine testPbc(this)

    class(test_main_type), intent(inout) :: this

    type(nucleus_type), dimension(2) :: nuclei
    type(atom_type), dimension(4) :: atoms
    real(dp), dimension(3,3) :: box
    real(dp), dimension(3,4) :: coords, ref_coords
    real(dp), dimension(3) :: force_dum, pivot
    real(dp), dimension(1,1,1) :: field
    type(multipoles_type) :: multipoles
    integer :: n_nucleus, n_atom
    type(quantum_fragment_type) :: quantum_fragment
    type(subsystem_type), dimension(1) :: subsystems
    type(density_type) :: density
    type(cell_type) :: cell

    box(:, :) = 0.0_dp
    box(1, 1) = 1.0_dp
    box(2, 2) = 1.0_dp
    box(3, 3) = 1.0_dp

    pivot = [box(1, 1), box(2, 2), box(3, 3)] * 0.5_dp

    coords(:, 1) = [0.8_dp, 0.2_dp, 0.2_dp]
    coords(:, 2) = [-0.2_dp, 1.2_dp, 1.2_dp]
    coords(:, 3) = [0.8_dp, 0.8_dp, 1.2_dp]
    coords(:, 4) = [0.8_dp, 1.2_dp, 0.8_dp]

    ref_coords(:, 1) = [0.8_dp, 0.2_dp, 0.2_dp]
    ref_coords(:, 2) = [-0.2_dp, 1.2_dp, 1.2_dp]
    ref_coords(:, 3) = [0.8_dp, -0.2_dp, 0.2_dp]
    ref_coords(:, 4) = [0.8_dp, 0.2_dp, -0.2_dp]

    do n_nucleus = 1, 2
        call nuclei(n_nucleus)%init(1, 1, 1, 1.0_dp, coords(:, n_nucleus), force_dum, .false.)
    enddo

    do n_atom = 1, 2
        call atoms(n_atom)%init(1, 1, 1, 1.0_dp, 1.0_dp, coords(:, n_atom), force_dum, multipoles, .true.)
    enddo
    do n_atom = 3, 4
        call atoms(n_atom)%init(1, 1, 1, 1.0_dp, 1.0_dp, coords(:, n_atom), force_dum, multipoles, .false.)
    enddo

    call cell%init(0, [1, 1, 1], [1, 1, 1], [0.0_dp, 0.0_dp, 0.0_dp], box)
    call density%init(0, field)
    call quantum_fragment%init(id=1, nuclei=nuclei, cell=cell, density=density)
    call subsystems(1)%init(0, 1.0_dp, atoms)
    call subsystems(1)%define_fragments(2, [0, 2, 4])
    call mimic_min_image(box, pivot, subsystems)

    do n_atom = 1, 4
        @assertEqual(ref_coords(:, n_atom), coords(:, n_atom), this%tolerance)
    end do

end subroutine testPbc

@Test(npes = [3])
subroutine testTriangularCount(this)

    class(test_main_type), intent(inout) :: this

    type(bond_type), dimension(5, 2) :: bonds
    integer, parameter :: n_codes = 2
    integer, dimension(2) :: bonds_pcode = [5, 3]
    integer :: n_code, n_bond, conn
    integer, dimension(:), allocatable :: n_connection
    integer, dimension(:, :), allocatable :: conn_map
    integer, dimension(8) :: ref_nconn = [2, 3, 3, 2, 1, 3, 3, 3]
    integer, dimension(3, 8) :: ref_connmap

    bonds(1, 1)%atom_i = 1
    bonds(1, 1)%atom_j = 2

    bonds(2, 1)%atom_i = 2
    bonds(2, 1)%atom_j = 3

    bonds(3, 1)%atom_i = 3
    bonds(3, 1)%atom_j = 4

    bonds(4, 1)%atom_i = 4
    bonds(4, 1)%atom_j = 5

    bonds(5, 1)%atom_i = 6
    bonds(5, 1)%atom_j = 7

    bonds(1, 2)%atom_i = 8
    bonds(1, 2)%atom_j = 9

    bonds(2, 2)%atom_i = 8
    bonds(2, 2)%atom_j = 10

    bonds(3, 2)%atom_i = 9
    bonds(3, 2)%atom_j = 10

    ref_connmap(:, 1) = [1, 2, 0]
    ref_connmap(:, 2) = [1, 2, 3]
    ref_connmap(:, 3) = [2, 3, 4]
    ref_connmap(:, 4) = [3, 4, 0]
    ref_connmap(:, 5) = [5, 0, 0]
    ref_connmap(:, 6) = [6, 7, 8]
    ref_connmap(:, 7) = [6, 7, 8]
    ref_connmap(:, 8) = [6, 7, 8]

    call mimic_count_trcnst(n_connection, conn_map, bonds, bonds_pcode)
    do n_bond = 1, size(ref_nconn)
        @assertEqual(ref_nconn(n_bond), n_connection(n_bond))
        do conn = 1, ref_nconn(n_bond)
            @assertEqual(ref_connmap(conn, n_bond), conn_map(conn, n_bond))
        end do
    end do

end subroutine testTriangularCount

@Test(npes = [3])
subroutine testTranslate(this)

    class(test_main_type), intent(inout) :: this

    type(nucleus_type), dimension(2) :: nuclei
    type(atom_type), dimension(4) :: atoms
    real(dp), dimension(3,3) :: box
    real(dp), dimension(3,4) :: coords, ref_coords
    real(dp), dimension(3) :: force_dum
    real(dp), dimension(3) :: shift
    real(dp), dimension(1,1,1) :: field
    type(multipoles_type) :: multipoles
    integer :: n_nucleus, n_atom
    type(quantum_fragment_type) :: quantum_fragment
    type(subsystem_type), dimension(1) :: subsystems
    type(density_type) :: density
    type(cell_type) :: cell

    box(:, :) = 0.0_dp
    box(1, 1) = 1.0_dp
    box(2, 2) = 1.0_dp
    box(3, 3) = 1.0_dp

    shift = [1.0_dp, -0.5_dp, 1.3_dp]

    coords(:, 1) = [ 0.8_dp, 0.2_dp, 0.2_dp]
    coords(:, 2) = [-0.2_dp, 1.2_dp, 1.2_dp]
    coords(:, 3) = [ 0.8_dp, 0.8_dp, 1.2_dp]
    coords(:, 4) = [ 0.8_dp, 1.2_dp, 0.8_dp]

    ref_coords(:, 1) = [1.8_dp, -0.3_dp, 1.5_dp]
    ref_coords(:, 2) = [0.8_dp,  0.7_dp, 2.5_dp]
    ref_coords(:, 3) = [1.8_dp,  0.3_dp, 2.5_dp]
    ref_coords(:, 4) = [1.8_dp,  0.7_dp, 2.1_dp]

    do n_nucleus = 1, 2
        call nuclei(n_nucleus)%init(1, 1, 1, 1.0_dp, coords(:, n_nucleus), force_dum, .false.)
    enddo

    do n_atom = 1, 2
        call atoms(n_atom)%init(1, 1, 1, 1.0_dp, 1.0_dp, coords(:, n_atom), force_dum, multipoles, .true.)
    enddo
    do n_atom = 3, 4
        call atoms(n_atom)%init(1, 1, 1, 1.0_dp, 1.0_dp, coords(:, n_atom), force_dum, multipoles, .false.)
    enddo

    call cell%init(0, [1, 1, 1], [1, 1, 1], [0.0_dp, 0.0_dp, 0.0_dp], box)
    call density%init(0, field)
    call quantum_fragment%init(id=1, nuclei=nuclei, cell=cell, density=density)
    call subsystems(1)%init(0, 1.0_dp, atoms)
    call subsystems(1)%define_fragments(2, [0, 2, 4])

    call mimic_translate(quantum_fragment, subsystems, shift)

    do n_atom = 1, 4
        @assertEqual(ref_coords(:, n_atom), coords(:, n_atom), this%tolerance)
    end do

end subroutine testTranslate

end module test_main
