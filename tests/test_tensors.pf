!
!    MiMiC: A Framework for Multiscale Modeling in Computational Chemistry
!    Copyright (C) 2021       Jógvan Magnus Haugaard Olsen,
!                             Viacheslav Bolnykh,
!                             Simone Meloni,
!                             Emiliano Ippoliti,
!                             and Ursula Röthlisberger.
!
!    This file is part of MiMiC.
!
!    MiMiC is free software: you can redistribute it and/or modify
!    it under the terms of the GNU Lesser General Public License as
!    published by the Free Software Foundation, either version 3 of
!    the License, or (at your option) any later version.
!
!    MiMiC is distributed in the hope that it will be useful, but
!    WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU Lesser General Public License for more details.
!
!    You should have received a copy of the GNU Lesser General Public License
!    along with this program.  If not, see <http://www.gnu.org/licenses/>.
!

module test_tensors

    use pFUnit

    use mimic_precision
    use mimic_tensors, only: initialize_tensors, terminate_tensors, &
                             convert_index, tensor_element, &
                             interaction_tensor, folded_interaction_tensor, &
                             detrace_tensor, &
                             mimic_factorial => factorial, &
                             mimic_double_factorial => double_factorial, &
                             mimic_binomial => binomial, &
                             mimic_trinomial => trinomial


    implicit none

    real(dp), dimension(0:12), parameter :: factorial = [         1.0_dp,        1.0_dp,         2.0_dp, &
                                                        &         6.0_dp,       24.0_dp,       120.0_dp, &
                                                        &       720.0_dp,     5040.0_dp,     40320.0_dp, &
                                                        &    362880.0_dp,  3628800.0_dp,  39916800.0_dp, &
                                                        & 479001600.0_dp]
    real(dp), dimension(0:12), parameter :: double_factorial = [     1.0_dp,    1.0_dp,     2.0_dp, &
                                                               &     3.0_dp,    8.0_dp,    15.0_dp, &
                                                               &    48.0_dp,  105.0_dp,   384.0_dp, &
                                                               &   945.0_dp, 3840.0_dp, 10395.0_dp, &
                                                               & 46080.0_dp]

    @testcase
    type, extends(testcase) :: test_tensor_type
    contains
        procedure :: setup
        procedure :: teardown
    end type test_tensor_type

contains

subroutine setup(this)

    class(test_tensor_type), intent(inout) :: this

    call initialize_tensors()

end subroutine setup

subroutine teardown(this)

    class(test_tensor_type), intent(inout) :: this

    call terminate_tensors()

end subroutine teardown

@test
subroutine test_factorial(this)

    class(test_tensor_type), intent(inout) :: this

    integer :: i

    do i = 0, 12
        @assertequal(mimic_factorial(i), factorial(i))
    end do

end subroutine test_factorial

@test
subroutine test_double_factorial(this)

    class(test_tensor_type), intent(inout) :: this

    integer :: i

    do i = 0, 12
        @assertequal(mimic_double_factorial(i), double_factorial(i))
    end do

end subroutine test_double_factorial

@test
subroutine test_binomial_coefficient(this)

    class(test_tensor_type), intent(inout) :: this

    integer :: i, j
    real(dp), dimension(0:12,0:12) :: binomial

    binomial(:,:) = 0.0_dp
    binomial(:,0) = 1.0_dp
    do i = 0, 12
        binomial(i,i) = 1.0_dp
    end do
    do i = 1, 12
        do j = 1, i-1
            binomial(i,j) = binomial(i-1,j-1) + binomial(i-1,j)
            @assertequal(mimic_binomial(i, j), binomial(i,j))
        end do
    end do

end subroutine test_binomial_coefficient

@test
subroutine test_trinomial_coefficient(this)

    class(test_tensor_type), intent(inout) :: this

    integer :: i, j, k
    real(dp) :: trinomial

    do i = 0, 12
        do j = 0, 12
            do k = 0, 12
                if (i+j+k > 12) then
                    cycle
                end if
                trinomial = factorial(i+j+k) / (factorial(i) * factorial(j) * factorial(k))
                @assertequal(mimic_trinomial(i, j, k), trinomial)
            end do
        end do
    end do

end subroutine test_trinomial_coefficient

!@test
!subroutine test_compute_tensor(this)
!
!    class(test_tensor_type), intent(inout) :: this
!
!
!end subroutine test_compute_tensor
!
!@test
!subroutine test_compute_folded_tensor(this)
!
!    class(test_tensor_type), intent(inout) :: this
!
!
!end subroutine test_compute_folded_tensor
!
!@test
!subroutine test_compute_tensor_element(this)
!
!    class(test_tensor_type), intent(inout) :: this
!
!
!end subroutine test_compute_tensor_element

@test
subroutine test_detrace_tensor(this)

    class(test_tensor_type), intent(inout) :: this

    real(dp) :: trace
    real(dp), dimension(6) :: traceless_quadrupole
    real(dp), dimension(10) :: traceless_octopole
    real(dp), dimension(6) :: quadrupole = [-3.9516312016_dp, -0.0561791973_dp,  0.0008348984_dp, &
                                            -4.5778807726_dp,  0.0000430036_dp, -5.0206878337_dp]
    real(dp), dimension(10) :: octopole = [-0.0150265431_dp, -0.0987395310_dp, -0.0000662324_dp, &
                                            0.0267925411_dp, -0.0002404801_dp,  0.0219905017_dp, &
                                            0.0316173819_dp, -0.0000537894_dp,  0.2063402594_dp, &
                                            0.0001754735_dp]

    traceless_quadrupole = quadrupole
    trace = (quadrupole(1) + quadrupole(4) + quadrupole(6)) / 3.0_dp
    traceless_quadrupole(1) = traceless_quadrupole(1) - trace
    traceless_quadrupole(4) = traceless_quadrupole(4) - trace
    traceless_quadrupole(6) = traceless_quadrupole(6) - trace

    ! detrace quadrupole
    call detrace_tensor(quadrupole)
    @assertequal(quadrupole, traceless_quadrupole, 1.0d-10)
    ! detrace traceless quadrupole to make sure it is not changed
    call detrace_tensor(quadrupole)
    @assertequal(quadrupole, traceless_quadrupole, 1.0d-10)

    traceless_octopole = octopole
    trace = (octopole(1) + octopole(4) + octopole(6)) / 5.0_dp
    traceless_octopole(1) = traceless_octopole(1) - 3.0_dp * trace
    traceless_octopole(4) = traceless_octopole(4) - trace
    traceless_octopole(6) = traceless_octopole(6) - trace
    trace = (octopole(2) + octopole(7) + octopole(9)) / 5.0_dp
    traceless_octopole(2) = traceless_octopole(2) - trace
    traceless_octopole(7) = traceless_octopole(7) - 3.0_dp * trace
    traceless_octopole(9) = traceless_octopole(9) - trace
    trace = (octopole(3) + octopole(8) + octopole(10)) / 5.0_dp
    traceless_octopole(3) = traceless_octopole(3) - trace
    traceless_octopole(8) = traceless_octopole(8) - trace
    traceless_octopole(10) = traceless_octopole(10) - 3.0_dp * trace

    ! detrace octopole
    call detrace_tensor(octopole)
    @assertequal(octopole, traceless_octopole, 1.0d-10)
    ! detrace traceless quadrupole to make sure it is not changed
    call detrace_tensor(octopole)
    @assertequal(octopole, traceless_octopole, 1.0d-10)

end subroutine

@test
subroutine test_convert_index(this)

    class(test_tensor_type), intent(inout) :: this

    integer :: order
    integer :: ref_index
    integer :: i, j, k

    do order = 1, 12
        ref_index = 1
        do i = order , 0, -1
            do j = order - i, 0, -1
                k = order - i - j
                @assertequal(convert_index(i, j, k), ref_index)
                ref_index = ref_index + 1
            end do
        end do
    end do

end subroutine test_convert_index

end module test_tensors
